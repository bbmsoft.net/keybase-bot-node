module.exports = function (RED) {
    function KeybaseChatNode(config) {
        RED.nodes.createNode(this, config);

        let bot = RED.nodes.getNode(config.bot);
        let conv = config.conversation;

        var node = this;
        node.on('input', function (msg) {
            let text = msg.payload;
            if (conv) {
                bot.sendMessage(text, conv);
            } else {
                let convId = msg.conversationId;
                if (convId) {
                    bot.sendMessage(text, convId);
                } else {
                    console.error("No conversation ID configured or attached to message, don't know where to send!")
                }
            }
        });
    }
    RED.nodes.registerType("keybase-out", KeybaseChatNode);
}
