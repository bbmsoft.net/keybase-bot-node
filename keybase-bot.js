const Bot = require('keybase-bot');
const storeNamespace = "node-red-bot";
const conversationIdKey = "conversationId";
const context = {};

module.exports = function (RED) {

    function KeybaseBotNode(n) {
        RED.nodes.createNode(this, n);
        context.handlers = new Map();
        this.registerCommand = registerCommand;
        this.sendMessage = sendMessage;
        this.botlabel = n.botlabel;

        this.on('close', function () {
            if (context.bot) {
                context.bot.deinit().catch(onError);
            }
        });

        startBot(this.credentials.username, this.credentials.paperkey);
    }

    async function startBot(username, paperkey) {

        if (!username) {
            console.log(`ERROR: username is not set.`)
            return;
        }
        if (!paperkey) {
            console.log(`ERROR: paperkey is not set.`)
            return;
        }

        let bot = new Bot();
        await bot.init(username, paperkey).then(() => {
            let info = bot.myInfo();
            console.log(`Keybase bot initialized with username ${info.username}.`);
            context.bot = bot;
        }).then(() => {
            registerCommands();
        }).then(() => {
            bot.chat.watchAllChannelsForNewMessages(onMessage, onError);
        }).catch(onError);
    }

    async function registerCommand(command, handler) {
        console.log("registering handler for command: " + command);
        context.handlers.set(command, handler);
        registerCommands();
    }

    async function sendMessage(msg, conversation) {

        if (!context.bot) {
            console.log("cannot send message, bot is not initialized yet");
            return;
        }

        console.log("sending message to keybase chat: " + msg);
        context.bot.chat.send(conversation, { body: `${msg}` }).catch(onError);
    }

    async function registerCommands() {

        if (context.handlers.size == 0) {
            console.log("nothing to register, no handlers present");
            return;
        }

        if (!context.bot) {
            console.log("cannot register commands, bot is not initialized");
            return;
        }

        let bot = context.bot;

        bot.chat.clearCommands().then(() => {
            let commands = [];
            context.handlers.forEach(function (handler, cmd) {

                let description = handler.description;
                let usage = handler.usage;

                console.log(`advertising command '${cmd}'`);

                commands.push({
                    name: cmd,
                    description: description,
                    usage: usage,
                });
            });
            bot.chat.advertiseCommands({
                advertisements: [
                    {
                        type: 'public',
                        commands: commands
                    }
                ],
            })
        }).catch(onError);
    }

    async function onMessage(msg) {

        if (msg.content.type !== 'text') {
            console.log("ignoring non-text message");
            return;
        }

        let body = msg.content.text.body;
        if (body) {
            console.log("received message: '" + body + "' in channel '" + msg.conversationId + "'");

            let info = context.bot.myInfo();
            if (body.startsWith(`@${info.username}`)) {
                sendHelpMessage(msg.conversationId);
            } else {
                dispatchMessage(msg);
            }
        } else {
            console.log("ignoring text message without body");
        }

    }

    async function sendHelpMessage(conversationId) {
        console.log("sending help to conversation " + conversationId);
        let help = `Hello!
I am the node-red keybase bot!

You can use me to send messages from node-red to a keybase chat and to handle commands from a keybase chat in node-red.

To send messages with the 'keybase-out' node, you will need to configure the conversation ID of the channel you want the node to write to. The conversation ID of THIS channel is:

${conversationId}

If you want a node to write to another channel, just install me in that channel as well and @mention me in a message there, then I will let you know which conversation ID that channel has.`
        sendMessage(help, conversationId);
    }

    function dispatchMessage(msg) {

        let body = msg.content.text.body;
        let cmd = body.substring(1, body.indexOf(" "));
        let handler = context.handlers.get(cmd);
        if (handler) {
            let arg = body.substr(cmd.length + 2);
            console.log(`dispatching msg '${arg}' to handler of command '${cmd}'`)
            handler.handleMessage({
                body: arg,
                conversationId: msg.conversationId,
                channel: msg.channel
            });
        }
    }

    function onError(e) {
        console.error(e);
    }

    RED.nodes.registerType("keybase-bot", KeybaseBotNode, {
        credentials: {
            username: { type: "text" },
            paperkey: { type: "password" }
        }
    });
}