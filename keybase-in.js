module.exports = function (RED) {

    function KeybaseChatNode(config) {
        RED.nodes.createNode(this, config);

        let node = this;

        node.command = config.command;
        node.description = config.description;
        node.usage = config.usage;
        node.handleMessage = msg => node.send({
            payload: msg.body,
            conversationId: msg.conversationId,
            channel: msg.channel,
        });

        let bot = RED.nodes.getNode(config.bot);
        if (bot) {
            node.bot = bot;
            if (node.command) {
                node.bot.registerCommand(node.command, node);
            }
        }
    }

    RED.nodes.registerType("keybase-in", KeybaseChatNode);
}
