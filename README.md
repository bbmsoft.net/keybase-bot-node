# Keybase Bot Node
A [node-red](https://github.com/node-red/node-red) I/O node for [keybase](https://keybase.io/) based on [keybase-bot](https://github.com/keybase/keybase-bot).

## What does it do?

It provides nodes for sending messages from node-red to keybase chats and for receiving messages and commands from keybase chats in node-red. The main purpose of this node is to enable everyone to build their own custom [keybase chat bot](https://keybase.io/blog/bots) without having to know any programming language.

## How does it work?

The keybase nodes use a keybase chat bot account to register commands that will then be available in any teams/chat channels to which the keybase bot is added. This way any flow defined in node-red can now be triggered from a keybase chat. Results or events can be sent back to the keybase chat.

## How do I get it?

Once a certain amount of maturity is reached, the nodes will be published to npm and can then simply be installed from within node-red.

Keybase needs to be installed in the device that runs node-red. On windows and x86 based linux boxes, the easiest way to achieve this is to install the official binaries provided by (keybase.io)[keybase.io].

On a Raspberry Pi or other ARM based devices, this can be a little tricky, since keybase does not provide any official ARM binaries. Keybase can however be installed from source. A guide for one way to install the minimum keybase components required by this node will be added to this readme in the future.

## What's missing?

A lot, probably. This is my first ever node and my first ever keybase bot and it is still in an early stage of development, so I've probably overlooked quite a few things so far. If you have experience with either and think you have something useful to contribute, feel free to contact me or issue a pull request!